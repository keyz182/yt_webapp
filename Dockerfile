# Sets up a container for the web based lab login
#
# VERSION               0.0.1

# At some point, more of this will be pushed into its own docker image

FROM      phusion/baseimage:0.9.13
MAINTAINER  Kacper Kowalik "xarthius.kk@gmail.com"

# RUN echo 'deb http://archive.ubuntu.com/ubuntu quantal main universe multiverse' > /etc/apt/sources.list
# RUN echo 'deb http://archive.ubuntu.com/ubuntu main universe multiverse' > /etc/apt/sources.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
RUN echo "deb https://get.docker.io/ubuntu docker main" > /etc/apt/sources.list.d/docker.list
RUN apt-get update -qq
RUN apt-get install -qy curl sudo python-dev python-pip git-core lxc-docker apparmor

# add a user
RUN useradd -D --shell=/bin/bash
RUN useradd -m user
RUN echo "user:secret" | chpasswd
RUN adduser user sudo
RUN gpasswd -a user docker
RUN sudo -u user mkdir /home/user/yt_serve

# supervisor process manager
RUN pip install supervisor
ADD supervisor/supervisord.conf /etc/supervisord.conf

RUN git clone https://bitbucket.org/nds-org/yt_webapp.git yt_serve
RUN pip install -r yt_serve/app/requirements.txt
ADD ./supervisor/yt_serve.conf /etc/supervisor.d/yt_serve.conf
ADD ./supervisor/docker.conf /etc/supervisor.d/docker.conf
ADD app/ /home/user/yt_serve/
EXPOSE 5000

# Install the magic wrapper.
ADD ./wrapdocker /usr/local/bin/wrapdocker
RUN chmod +x /usr/local/bin/wrapdocker

VOLUME /var/lib/docker
CMD supervisord -n -c /etc/supervisord.conf
