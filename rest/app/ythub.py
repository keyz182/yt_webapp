#!/usr/bin/env python
import sys
import os
import requests
import types
from yt.startup_tasks import parser, subparsers
from yt.extern.six import add_metaclass
from yt.funcs import ensure_list


url = 'http://localhost:5888'


def _fix_ds(arg):
    if os.path.isdir("%s" % arg) and \
        os.path.exists("%s/%s" % (arg,arg)):
        ds = load("%s/%s" % (arg,arg))
    elif os.path.isdir("%s.dir" % arg) and \
        os.path.exists("%s.dir/%s" % (arg,arg)):
        ds = load("%s.dir/%s" % (arg,arg))
    elif arg.endswith(".index"):
        ds = load(arg[:-10])
    else:
        ds = load(arg)
    return ds


def _add_arg(sc, arg):
    if isinstance(arg, types.StringTypes):
        arg = _common_options[arg].copy()
    argc = dict(arg.items())
    argnames = []
    if "short" in argc: argnames.append(argc.pop('short'))
    if "longname" in argc: argnames.append(argc.pop('longname'))
    sc.add_argument(*argnames, **argc)


class YTCommandSubtype(type):
    def __init__(cls, name, b, d):
        type.__init__(cls, name, b, d)
        if cls.name is not None:
            names = ensure_list(cls.name)
            for name in names:
                sc = subparsers.add_parser(name,
                    description = cls.description,
                    help = cls.description)
                sc.set_defaults(func=cls.run)
                for arg in cls.args:
                    _add_arg(sc, arg)

@add_metaclass(YTCommandSubtype)
class YTCommand(object):
    args = ()
    name = None
    description = ""
    aliases = ()
    ndatasets = 1

    @classmethod
    def run(cls, args):
        self = cls()
        # Some commands need to be run repeatedly on datasets
        # In fact, this is the rule and the opposite is the exception
        # BUT, we only want to parse the arguments once.
        if cls.ndatasets > 1:
            self(args)
        else:
            ds_args = getattr(args, "ds", [])
            if len(ds_args) > 1:
                datasets = args.ds
                for ds in datasets:
                    args.ds = ds
                    self(args)
            elif len(ds_args) == 0:
                datasets = []
                self(args)
            else:
                args.ds = getattr(args, 'ds', [None])[0]
                self(args)


class YTRunCmd(YTCommand):
    args = (dict(short="file", type=str), )
    name = "run"
    description = \
        """
        Run yt script in the cloud

        """

    def __call__(self, args):
        filename = args.file
        if not os.path.isfile(filename):
            raise IOError(filename)
        if not filename.endswith(".py"):
            print "File must be an Python script!"
            return 1
        files = [('filearg', (os.path.basename(filename),
                              open(filename, 'rb'),
                              'text/plain'))]   # Automagic?
        r = requests.post(url + '/upload', files=files)
        print r.text


class YTDowloadCmd(YTCommand):
    args = (dict(short="nid", type=int), )
    name = "get_results"
    description = \
        """
        Download results from the cloud
        """

    def __call__(self, args):
        with open('output.tar', 'w') as handle:
            response = requests.get(url + '/results/%i' % args.nid,
                                    stream=True)

            if not response.ok:
                raise IOError  # Something went wrong

            for block in response.iter_content(1024):
                if not block:
                    break

                handle.write(block)


def run_main():
    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__": run_main()
