import yt
import numpy as np

print yt.__version__

arr = np.random.random(size=(64, 64, 64))
data = dict(density=(arr, "g / cm ** 3"))
bbox = np.array([[-1.5, 1.5], [-1.5, 1.5], [-1.5, 1.5]])
ds = yt.load_uniform_grid(data, arr.shape, length_unit="Mpc", bbox=bbox,
                          nprocs=12)
slc = yt.SlicePlot(ds, "z", ["density"])
slc.set_cmap("density", "Blues")
slc.annotate_grids(cmap=None)
slc.save()
