import os
import uuid
import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.template
import docker
import threading
import json

import tasks
import tcelery

tcelery.setup_nonblocking_producer()


__UPLOADS__ = "uploads/"
BASE_IMAGE = 'ytproject/yt-devel'
BASE_IMAGE_TAG = 'hublaunch'
CONTAINER_STORAGE = "/tmp/containers.json"
name = "xarthisius"
workdir = "results"

lock = threading.Lock()


def get_containers():
    # TODO should this be reset at startup?
    container_store = CONTAINER_STORAGE
    if not os.path.exists(container_store):
        with lock:
            json.dump({}, open(container_store, 'wb'))
        return None
    return json.load(open(container_store, 'rb'))


class StatsHandler(tornado.web.RequestHandler):

    def get(self):
        try:
            old_jobs = get_containers()[name]
        except KeyError:
            old_jobs = []
        for image in dcli.containers():
            if image['Id'] in old_jobs:
                old_jobs.remove(image['Id'])
        self.write(loader.load("stats.html").generate(dcli=dcli,
                                                      old_jobs=old_jobs))


class ResultsHandler(tornado.web.RequestHandler):

    def get(self, nid):
        finished_jobs = get_containers()[name]
        raw = dcli.copy(finished_jobs[int(nid)], '/results')
        self.write(raw.read())


class StatusHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self, cont_id):
        tasks.get_job_state_docker.apply_async(args=[cont_id],
                                               callback=self.on_result)

    def on_result(self, response):
        self.write(str(response.result))
        self.finish()


class StdoutHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self, cont_id):
        tasks.get_job_stdout_docker.apply_async(args=[cont_id],
                                                callback=self.on_result)

    def on_result(self, response):
        self.write(str(response.result))
        self.finish()


class MetricHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self, cont_id):
        tasks.get_job_metric_docker.apply_async(args=[cont_id],
                                                callback=self.on_result)

    def on_result(self, response):
        self.write(str(response.result))
        self.finish()


class UploadHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def post(self):
        fileinfo = self.request.files['filearg'][0]
        fname = fileinfo['filename']
        extn = os.path.splitext(fname)[1]
        cname = str(uuid.uuid4()) + extn
        filename = os.path.join(__UPLOADS__, cname)
        with open(filename, 'w') as fh:
            fh.write(fileinfo['body'])

        tasks.create_job_docker.apply_async(args=[os.path.abspath(filename)],
                                            callback=self.on_result)

    def on_result(self, response):
        self.write(str(response.result))
        self.finish()


class Userform(tornado.web.RequestHandler):

    def get(self):
        self.render(os.path.join(template_dir, "fileuploadform.html"))


application = tornado.web.Application([
    (r"/results/([0-9]+)", ResultsHandler),
    (r"/stats", StatsHandler),
    (r"/job/(\w+)/status", StatusHandler),
    (r"/job/(\w+)/metric", MetricHandler),
    (r"/job/(\w+)/stdout", StdoutHandler),
    (r"/upload", UploadHandler),
    (r"/", Userform),
])

template_dir = os.path.join(os.getcwd(), "templates")
bind_src = os.path.join(os.getcwd(), __UPLOADS__)
if not os.path.isdir(bind_src):
    os.mkdir(bind_src)
loader = tornado.template.Loader(template_dir)
dcli = docker.Client(base_url='unix://var/run/docker.sock',
                     version='1.6',
                     timeout=10)
dcli.pull(BASE_IMAGE, tag=BASE_IMAGE_TAG)

if __name__ == "__main__":
    application.listen(5888)
    tornado.ioloop.IOLoop.instance().start()
