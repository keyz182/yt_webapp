#!/bin/bash
cd /home/user/yt_serve

if [[ -z "$HOSTNAME" ]]
then
    HOSTNAME = "testing"
fi

sudo -u user RABBITMQ_PORT_5672_TCP_ADDR=$RABBITMQ_PORT_5672_TCP_ADDR RABBITMQ_PORT_5672_TCP_PORT=$RABBITMQ_PORT_5672_TCP_PORT HOSTNAME=$HOSTNAME celery worker --app=tasks -l info -n $HOSTNAME -Q common,$HOSTNAME
