yt Hub Web interface
====================

      python main.py
      celery worker -A backend.v1 -Q payment_default1 --concurrency 2 --loglevel=INFO 

      curl -F "image_name=ytproject/yt-devel" -F "image_tag=hublaunch" -F "file=@script.py" "http://localhost:8888/v1/upload"
      curl "http://localhost:8888/v1/stdout?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/diff?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/metric?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/download?cont_id=997231c3ce6&path=/results"
