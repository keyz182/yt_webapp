import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.escape

from tornado import gen
from tornado.web import asynchronous


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('main')


class AppHandler(tornado.web.RequestHandler):
    @asynchronous
    @gen.coroutine
    def get(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.get_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.get_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result)
            # self.write(response.result.replace('"', '').replace("'", '"'))
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def post(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.post_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        kwargs['files'] = self.request.files
        kwargs['form'] = self.request.arguments
        response = yield self.task_handler.post_one(0,
                                                    self.request.arguments,
                                                    **kwargs)
        if response.result:
            self.write(response.result.replace('"', '').replace("'", '"'))
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def put(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.put_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.put_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result.replace('"', '').replace("'", '"'))
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def delete(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.delete_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.delete_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result.replace('"', '').replace("'", '"'))
        else:
            self.raise404()
        self.finish()
